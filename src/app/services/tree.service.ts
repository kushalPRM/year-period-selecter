import { Injectable } from '@angular/core';


@Injectable({
    providedIn: 'root'
})

export class TreeService {

    finalResult: any;
    distinctedYears: Array<string> = [];
    metaDatas: any;
    selectedYear: any;
    getChildrensValue: any;
    //latestFinancialyear: any;
    parentError: any = "";
    quarterError: any = "";
    ytdQuarterError: any = "";
    recordedItems: any;

    maxYearErrorCode: any = "101";
    maxQuarterErrorCode: any = "102";
    maxYtdQuarterErrorCode: any = "103";
    private yearArray: Array<string> = [];
    range = (start: any, end: any = new Date().getFullYear()) => Array.from({ length: (end + 1 - start) }, (v, k) => k + start);

    getPastYear(endyear, years) {

        let date = new Date();
        let currentYear = endyear;
        date.setFullYear(currentYear - (years - 1));
        return date.getFullYear();
    }

    getTrees(metaDatas: any, currentSystemYear: any, totalYears: any = 10, latestFinancialYear?: any, recordedItems?: any) {

        const treeArray = [];
        const date = new Date();
        const currentYear = (currentSystemYear && currentSystemYear != "") ? currentSystemYear : date.getFullYear();
        const pastYear = this.getPastYear(currentYear, totalYears);
        this.yearArray = this.range(pastYear, currentYear);
        this.yearArray.reverse();
        this.recordedItems = recordedItems;
        // treeArray.push({metadata: [metaDatas.metadata]});
        // ******* Validation  ********* //

        this.metaDatas = metaDatas.metadata;

        const annualStatus = (
            metaDatas.metadata.maxYear && metaDatas.metadata.maxYear > 0 ||
            metaDatas.metadata.minYear && metaDatas.metadata.minYear > 0
        ) ? false : true;

        const quarterStatus = (
            metaDatas.metadata.maxQuarter && metaDatas.metadata.maxQuarter > 0 ||
            metaDatas.metadata.minQuarter && metaDatas.metadata.minQuarter > 0
        ) ? false : true;
        // ******* Validation ********* //



        for (let i = 0; i < this.yearArray.length; i++) {

            const treeIn = {
                text: this.yearArray[i].toString(),
                value: this.yearArray[i].toString(),
                disabled: annualStatus,
                children: [
                    {
                        text: 'Annualize',
                        value: 'Annualize',
                        hidden: this.removeAnnualize(currentSystemYear, metaDatas, this.yearArray[i]),
                        checked: false,
                        disabled: false// this.disbaleAnualize(metaDatas)
                    },
                    {
                        text: 'Annual',
                        value: 'Annual',
                        checked: false,
                        hidden: !this.removeAnnualize(currentSystemYear, metaDatas, this.yearArray[i]),
                        disabled: annualStatus,
                    },
                    {
                        text: 'Quaters',
                        value: 'Quaters',
                        checked: false,
                        disabled: this.disabledQuarters(metaDatas),
                    },
                    {
                        text: 'YTD-Q',
                        value: 'YTD-Q',
                        checked: false,
                        disabled: this.disabledYtdQ(metaDatas),
                    },
                    {
                        text: 'LMT-Q',
                        value: 'LMT-Q',
                        checked: false,
                        disabled: this.disabledLmtq(metaDatas),
                    }
                ]
            };

            treeArray.push(treeIn);
            // treeIn.correctChecked();
        }

        return treeArray;
    }

    disbaleAnualize(metaDatas) {
        if (metaDatas.metadata.enableAnnualize && metaDatas.metadata.enableAnnualize > 0) {
            return false;
        } else {
            return true;
        }
    }

    defaultYearChecked(nodeValue, metaDatas, latestFinancialYear) {

        const defaultYearChecked = (
            latestFinancialYear != undefined
            && (
                metaDatas.metadata.maxYear && metaDatas.metadata.maxYear > 0 || metaDatas.metadata.minYear && metaDatas.metadata.minYear > 0
            )
        ) ? this.range(
            this.getPastYear(latestFinancialYear, metaDatas.metadata.minYear),
            latestFinancialYear
        ).includes(nodeValue) : false;
        return defaultYearChecked;
    }

    removeAnnualize(currentSystemYear, metaDatas, nodeValue) {
        const d = new Date();
        if (nodeValue === d.getFullYear() && metaDatas.metadata.enableAnnualize === 1) {
            return false;
        } else {
            return true;
        }

    }


    getLatestFinancialYears(currentSystemYear, totalYears, metaDatas) {

        return this.range(this.getPastYear(currentSystemYear, (totalYears - metaDatas.metadata.minYear + 1)), currentSystemYear);
    }

    disabledQuarters(metaDatas) {
        if (metaDatas.metadata.enableQuarter && metaDatas.metadata.enableQuarter === 1) {
            return false;
        } else {
            return true;
        }

    }

    disabledLmtq(metaDatas) {

        if (metaDatas.metadata.enableLmtQuarter && metaDatas.metadata.enableLmtQuarter === 1) {
            return false;
        } else {
            return true;
        }
    }

    disabledYtdQ(metaDatas) {
        if (metaDatas.metadata.enableYtdQuarter && metaDatas.metadata.enableYtdQuarter === 1) {
            return false;
        } else {
            return true;
        }
    }

    getYtdQuarterStatus(metaDatas, value) {
        const ytdQuarterStatus = (
            // tslint:disable-next-line: max-line-length
            (metaDatas.metadata.maxYtdQuarter && metaDatas.metadata.maxYtdQuarter > 0 || metaDatas.metadata.minYtdQuarter && metaDatas.metadata.minYtdQuarter > 0) && !metaDatas.metadata.ytd.includes(value)
        ) ? false : true;
        return ytdQuarterStatus;
    }

    setMinYear(metaDatas, treeArray) {
        treeArray.forEach((e, index) => {
            e.children.forEach((child, position) => { child.parentId = index; });
        });

        this.selectedYear = treeArray[0].text;
        this.getChildrensValue = treeArray[0].children;

        if (typeof this.recordedItems !== 'undefined' && this.recordedItems !== null) {
            return this.setTreeByResponseData(treeArray);
        } else {
            for (let i = 0; i < this.metaDatas.minYear; i++) {
                treeArray[i].checked = true;
                if (!treeArray[i].children[0].disabled && !treeArray[i].children[0].hidden) {
                    treeArray[i].children[0].checked = true;
                } else {
                    treeArray[i].children[1].checked = true;
                }
            }
            this.finalResult = this.getFinalData(treeArray);
            return treeArray;
        }
    }

    private setTreeByResponseData(treeArray) {
        const date = new Date();
        const currentYear = date.getFullYear();
        for (const tree of treeArray) {
            for (const periodArr of this.recordedItems) {
                if ('period' in periodArr) {
                    if (tree.value === periodArr.period) {
                        let isAnyChildChecked = false;
                        if ('children' in tree) {
                            let setAnnualIndexForPastYear = false;
                            for (let j = 0; j < tree.children.length; j++) {
                                if (j === 1 && setAnnualIndexForPastYear) {
                                    if (!tree.children[j].disabled) {
                                        tree.children[j].checked = true;
                                        isAnyChildChecked = true;
                                    } else {
                                        tree.children[j].checked = false;
                                    }
                                } else {
                                    if ('type' in periodArr) {
                                        for (const periodType of periodArr.type) {
                                            // tslint:disable-next-line: max-line-length
                                            if ('value' in periodType && tree.children[j].text === periodType.value) {
                                                if (!tree.children[j].disabled) {
                                                    tree.children[j].checked = true;
                                                    isAnyChildChecked = true;
                                                } else {
                                                    tree.children[j].checked = false;
                                                }
                                                if (j === 0 && currentYear !== tree.value) {
                                                    setAnnualIndexForPastYear = true;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if (isAnyChildChecked) {
                            tree.checked = true;
                        }
                    }
                }
            }
        }
        this.finalResult = this.getFinalData(treeArray);
        return treeArray;
    }

    private getFinalData(data) {
        const finalResultData = { requestedPeriods: [] };
        data.filter((e) => {
            if (e.checked === true) {
                e.children.filter((d) => {
                    if (d.checked && !d.hidden) {
                        finalResultData.requestedPeriods.push({
                            period: e.value,
                            type: d.text
                        });
                    }
                });
            }
        });
        return finalResultData;
    }
}
