import { Component, AfterViewInit, Output, OnInit, EventEmitter, ElementRef, ViewChild } from '@angular/core';
import { TreeService } from './services/tree.service';
import { YearSelectComponent } from './year-select/year-select.component';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent implements OnInit, AfterViewInit {
  title = 'yearSelecter';
  items: any;
  requestedPeriods: [];
  currentYear = 2019;
  latestFinancialYear = 2018;
  recordedItems: any = JSON.parse('[{"period":"2019","type":[{"name":"A","value":"Annualize"},{"name":"LMT-Q","value":"LMT-Q"}]},{"period":"2018","type":[{"name":"A","value":"Annual"},{"name":"LMT-Q","value":"LMT-Q"}]},{"period":"2016","type":[{"name":"A","value":"Annual"},{"name":"LMT-Q","value":"LMT-Q"}]}]');
  metaDatas = {
    "metadata": {
      "latestFinancialYear": 2019,
      "maxYear": 4,
      "minYear": 2,
      "minQuarter": 3,
      "enableYtdQuarter": 1,
      "enableQuarter": 1,
      "enableLmtQuarter": 1,
      "enableAnnualize": 1,
      //"maxQuarter": 4,
      //"minYtdQuarter": 1,
      //"maxYtdQuarter": 2,
      "templateType": "Historical",
      "businessSegment": "SME"
    }
  };

  constructor(public tree: TreeService) { }
  ngOnInit() {
    this.items = this.tree.getTrees(this.metaDatas, this.currentYear, 10, this.latestFinancialYear, this.recordedItems);
    console.log(this.items);
    this.tree.setMinYear(this.metaDatas, this.items);
  }
  ngAfterViewInit() {

    // this.yearSelecter
  }

}

