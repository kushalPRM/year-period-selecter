import { Component, OnInit } from '@angular/core';
import { TreeService } from '../services/tree.service';

@Component({
  selector: 'app-selector',
  templateUrl: './selector.component.html',
  styleUrls: ['./selector.component.css']
})
export class SelectorComponent implements OnInit {
  currentYear = 2019;
  year = '2019';
  period = '1';

  items: any;
  latestFinancialYear = 2018;
  requestedPeriods: [];
  recordedItems: any;

  title = 'yearSelecter';
  // items: any;
  // requestedPeriods: [];
  // currentYear = 2019;
  // latestFinancialYear = 2018;
  // recordedItems: any = JSON.parse('[{"period":"2019","type":[{"name":"A","value":"Annualize"},{"name":"LMT-Q","value":"LMT-Q"}]},{"period":"2018","type":[{"name":"A","value":"Annual"},{"name":"LMT-Q","value":"LMT-Q"}]},{"period":"2016","type":[{"name":"A","value":"Annual"},{"name":"LMT-Q","value":"LMT-Q"}]}]');
  metaDatas = {
    "metadata": {
      "latestFinancialYear": 2019,
      "maxYear": 4,
      "minYear": 2,
      "minQuarter": 3,
      "enableYtdQuarter": 1,
      "enableQuarter": 1,
      "enableLmtQuarter": 1,
      "enableAnnualize": 1,
      //"maxQuarter": 4,
      //"minYtdQuarter": 1,
      //"maxYtdQuarter": 2,
      "templateType": "Historical",
      "businessSegment": "SME"
    }
  };

  constructor(public tree: TreeService) { }

  ngOnInit() {
    this.items = this.tree.getTrees(this.metaDatas, this.currentYear, 10, this.latestFinancialYear, this.recordedItems);
    console.log(this.items);
    this.tree.setMinYear(this.metaDatas, this.items);
  }

  yearChanged() {
    // tslint:disable-next-line: radix
    this.currentYear = parseInt(this.year);
    this.items = null;
    this.items = this.tree.getTrees(this.metaDatas, this.currentYear, 10, this.latestFinancialYear, this.recordedItems);
    console.log(this.items);
    this.tree.setMinYear(this.metaDatas, this.items);
  }

  periodChanged() {
    const period = this.period;
    if (period === '1') {
        this.metaDatas.metadata.enableYtdQuarter = 1;
        this.metaDatas.metadata.enableQuarter = 1;
        this.metaDatas.metadata.enableLmtQuarter = 1;
        this.metaDatas.metadata.enableAnnualize = 1;
    } else if (period === '2') {
      this.metaDatas.metadata.enableYtdQuarter = 0;
      this.metaDatas.metadata.enableQuarter = 1;
      this.metaDatas.metadata.enableLmtQuarter = 1;
      this.metaDatas.metadata.enableAnnualize = 1;
    } else if (period === '3') {
      this.metaDatas.metadata.enableYtdQuarter = 1;
      this.metaDatas.metadata.enableQuarter = 1;
      this.metaDatas.metadata.enableLmtQuarter = 1;
      this.metaDatas.metadata.enableAnnualize = 0;
    } else if (period === '4') {
      this.metaDatas.metadata.enableYtdQuarter = 1;
      this.metaDatas.metadata.enableQuarter = 0;
      this.metaDatas.metadata.enableLmtQuarter = 1;
      this.metaDatas.metadata.enableAnnualize = 1;
    } else if (period === '5') {
      this.metaDatas.metadata.enableYtdQuarter = 1;
      this.metaDatas.metadata.enableQuarter = 1;
      this.metaDatas.metadata.enableLmtQuarter = 0;
      this.metaDatas.metadata.enableAnnualize = 1;
    }

    this.items = this.tree.getTrees(this.metaDatas, this.currentYear, 10, this.latestFinancialYear, this.recordedItems);
    console.log(this.items);
    this.tree.setMinYear(this.metaDatas, this.items);

  }

}
