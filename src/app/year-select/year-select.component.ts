import { Component, EventEmitter, Input, Output, ViewChild, ElementRef, OnChanges, AfterViewInit } from '@angular/core';
import { TreeService } from '../services/tree.service';

@Component({
    selector: 'year-select',
    templateUrl: './year-select.component.html',
    styleUrls: ['./year-select.component.css']
})
export class YearSelectComponent implements OnChanges, AfterViewInit {
    finalResult: Array<any> = [];
    gettreedata: any;
    getChildrensValue: any;
    selectedYear: string;
    selectedChild = [];
    @Input()
    treeServiceData: any;
    periodTypeStateInfo = [];
    selectedPeriodType = [];

    constructor(private treeService: TreeService) { }

    ngOnChanges() {

        this.gettreedata = this.treeServiceData;
        this.selectedYear = this.treeService.selectedYear;
        this.getChildrensValue = this.treeService.getChildrensValue;
        this.setPeriodStateInfo();
    }
    ngAfterViewInit() {
        //document.querySelectorAll('.year-select-left li a.label')[0].classList.add('active');
    }
    validateMaxYear(data, event) {
        let i = 0;
        data.filter((e) => {
            if (e.checked) {
                i++;
            }
        });
        if (i >= 4) {
            alert('You have reached maximum year selection limit');
            data.filter((d) => {
                if (!d.checked) {
                    d.disabled = true;

                }
            });
        } else {
            data.filter((d) => {
                if (!d.checked) {
                    d.disabled = false;

                }
            });
        }
    }

    getChidlrenPeridos(event, i, text) {
        if (!event.target.classList.contains('disabled')) {
            document.querySelectorAll('.year-select-wrapper li a').forEach((e) => {
                if (event.target === e) {
                    e.classList.add('active');
                } else {
                    e.classList.remove('active');
                }
            });
            this.selectedYear = text;
            this.getChildrensValue = this.gettreedata[i].children;
            this.getChildrensValue.forEach((data, index) => {
                data.parentId = i;
            });
        }

    }
    onYearSelect(e, id, checked) {
        this.perofrmYearSettings(id, checked);
        this.validateMaxYear(this.gettreedata, e);
        this.getFinalData(this.gettreedata);
    }

    checkChildParent(pId) {
        let selectedChild = this.gettreedata[pId].children.filter((data) => {
            if (data.checked === true) {
                return data;
            };
        });

        if (selectedChild.length > 0) {
            if (!this.gettreedata[pId].checked) {
                this.perofrmYearSettings(pId, false);
                this.validateMaxYear(this.gettreedata, null);
            }

        } else {
            this.perofrmYearSettings(pId, true);
            this.validateMaxYear(this.gettreedata, null);
        }

        this.uncheckOtherYears(pId);
    }

    checkUncheckSimilarNode(pid, fulltree, status: Boolean, nodeName: string) {
        fulltree.forEach((elem) => {
            elem.children.forEach((child) => {
                if ((!elem.disabled && elem.checked) && (!child.disabled && child.value === nodeName)) {
                    child.checked = status;
                }
            })
        });
    }

    onPeriodsSelect(e, i, checked, pId, nodeName) {
        if (i === 0 || i === 1) {
            if (!this.gettreedata[pId].children[0].hidden) {
                this.periodTypeStateInfo[pId][1] = !checked;
                this.gettreedata[pId].children[1].checked = !checked;
                this.selectedPeriodType[1] = !checked;
            } else {
                this.periodTypeStateInfo[pId][0] = !checked;
                this.gettreedata[pId].children[0].checked = !checked;
                this.selectedPeriodType[0] = !checked;
            }
        } else {
            this.periodTypeStateInfo[pId][i] = !checked;
            this.gettreedata[pId].children[i].checked = !checked;
            this.selectedPeriodType[i] = !checked;
        }

        this.performPeriodSettings(pId, i);

        this.checkUncheckSimilarNode(pId, this.gettreedata, !checked, nodeName);
        this.checkChildParent(pId);
        this.getFinalData(this.gettreedata);
    }


    // // extract result
    getFinalData(data) {
        const finalResultData = { requestedPeriods: [] };
        data.filter((e) => {
            if (e.checked === true) {
                e.children.filter((d) => {
                    if (d.checked && !d.hidden) {
                        finalResultData.requestedPeriods.push({
                            period: e.value,
                            type: d.text
                        });
                    }
                });
            }
        });
        // emit final result from here
        this.treeService.finalResult = finalResultData;
        // this.finalResultEmitter.emit({ 'result': this.result });
        console.log(finalResultData);
    }

    private setPeriodStateInfo(): void {
        this.periodTypeStateInfo = [];
        this.selectedPeriodType = [];
        if (this.gettreedata && this.gettreedata.length > 0) {
            for (let i = 0; i < this.gettreedata.length; i++) {
                const tempPeriodArr = [];
                for (let j = 0; j < this.gettreedata[i].children.length; j++) {
                    tempPeriodArr[j] = this.gettreedata[i].children[j].disabled ? false : this.gettreedata[i].children[j].checked;
                    if (j > 1 && this.gettreedata[i].children[j].checked && !(j in this.selectedPeriodType)) {
                        // tslint:disable-next-line: max-line-length
                        this.selectedPeriodType[j] = this.gettreedata[i].children[j].disabled ? false : this.gettreedata[i].children[j].checked;
                    }
                }
                this.periodTypeStateInfo[i] = tempPeriodArr;
            }
        }
    }

    private perofrmYearSettings(id, checked: Boolean): void {
        let selectedAnnualPeriodIndex = -1;
        if (checked) {
            this.gettreedata[id].checked = false;
            let i = 0;
            this.gettreedata[id].children.forEach((data) => {
                data.checked = false;
                this.periodTypeStateInfo[id][i] = false;
                i++;
            });
        } else {
            this.gettreedata[id].checked = true;
            let childLength = this.gettreedata[id].children.length;
            if (childLength > 0) {
                for (let i = 0; i < childLength; i++) {
                    if (i == 0 || i == 1) {
                        let checkedState = this.gettreedata[id].children[i].hidden ? false : true;
                        this.gettreedata[id].children[i].checked = checkedState;
                        this.periodTypeStateInfo[id][i] = checkedState;
                        if (!this.gettreedata[id].children[i].hidden) {
                            selectedAnnualPeriodIndex = i;
                        }
                    } else {
                        if (i in this.selectedPeriodType) {
                            this.gettreedata[id].children[i].checked = this.selectedPeriodType[i];
                            this.periodTypeStateInfo[id][i] = this.selectedPeriodType[i];
                        } else {
                            this.gettreedata[id].children[i].checked = this.periodTypeStateInfo[id][i];
                        }
                    }
                }
            }
            this.setSettingForOtherYears(id, selectedAnnualPeriodIndex);
        }
    }

    private setSettingForOtherYears(id, selectedAnnualPeriodIndex): void {
        for (let i = 0; i < this.gettreedata.length; i++) {
            if (i != id && ('checked' in this.gettreedata[i] && this.gettreedata[i].checked)) {
                for (let j = 0; j < this.gettreedata[i].children.length; j++) {
                    if (j == 0 || j == 1) {
                        if (selectedAnnualPeriodIndex == j) {
                            let checkedState = this.gettreedata[i].children[j].hidden ? false : true;
                            this.gettreedata[i].children[j].checked = checkedState;
                            this.periodTypeStateInfo[i][j] = checkedState;
                        }
                    } else {
                        this.gettreedata[i].children[j].checked = this.periodTypeStateInfo[id][j];
                        this.periodTypeStateInfo[i][j] = this.periodTypeStateInfo[id][j];
                    }
                }
            }
        }
    }

    private performPeriodSettings(id, index): void {
        for (let i = 0; i < this.gettreedata.length; i++) {
            if (i !== id) {
                if ('checked' in this.gettreedata[i] && this.gettreedata[i].checked) {
                    if (this.gettreedata[i].children.length > 0) {
                        this.gettreedata[i].children[index].checked = this.periodTypeStateInfo[id][index];
                    }
                }
            }
        }
    }

    private uncheckOtherYears(id): void {
        for (let i = 0; i < this.gettreedata.length; i++) {
            if (i != id && ('checked' in this.gettreedata[i] && this.gettreedata[i].checked)) {
                let selectedChild = this.gettreedata[i].children.filter((data) => {
                    if (!('hidden' in data) || ('hidden' in data && !data.hidden)) {
                        if (data.checked === true) {
                            return data;
                        };
                    }
                });

                if (selectedChild.length === 0) {
                    this.gettreedata[i].checked = false;
                }
            }
        }

    }
}
